package com.qf.bootes;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.bootes.entity.Book;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import static org.elasticsearch.client.RequestOptions.DEFAULT;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class BookDataTest {
    private static final String INDEX="book";

    @Resource
    RestHighLevelClient client;

    @Test
    void addBookWithJson() throws IOException {
        //准备数据
        Book book = new Book("1", "游山西村", "陆游", 1000, new Date(), "山重水复疑无路，柳暗花明又一村");
        IndexRequest request = new IndexRequest(INDEX);
        //指定ID
        request.id(book.getId());
        //指定source数据[json]
        //把实体类的book转为json字串
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(book);
        request.source(json, XContentType.JSON);
        //发送索引[新增]数据的请求
        IndexResponse response = client.index(request, DEFAULT);
        System.out.println(response);
    }

    @Test
    void addBookWithMap() throws IOException {
        //准备数据
        String id="2";
        Map<String, Object> book = new HashMap<>();
        book.put("id",id);
        book.put("name","红楼梦");
        book.put("author","曹雪芹");
        book.put("count",2000);
        book.put("on-sale","2023-06-13");
        book.put("descr","红楼梦的描述");

        IndexRequest request = new IndexRequest(INDEX);
        //指定ID
        request.id(id);
        //指定source数据[map]
        request.source(book);
        //发送索引[新增]数据的请求
        IndexResponse response = client.index(request, DEFAULT);
        System.out.println(response);

    }

    @Test
    void updateBook() throws IOException {
        UpdateRequest update = new UpdateRequest(INDEX, "2");
        //修改ID是2的数据的descr
        Map<String,Object> map = new HashMap<>();
        map.put("descr","一把辛酸泪，满纸荒唐言");
        update.doc(map);
        //发送更新数据的请求
        UpdateResponse response = client.update(update, DEFAULT);
        System.out.println(response);
    }

    @Test
    void deleteBookById() throws IOException {
        DeleteRequest request = new DeleteRequest(INDEX, "2");
        DeleteResponse response = client.delete(request, DEFAULT);
        System.out.println(response);
    }

    //批量新增数据
    @Test
    void bulkAddBooks() throws IOException {
        //准备数据
        Book book1 = new Book("11","西游记","吴承恩",2000,new Date(),"神话小说有可能是真的呢");
        Book book2 = new Book("12","红楼梦","曹雪芹",2000,new Date(),"红楼梦的描述");
        Book book3 = new Book("13","三国演义","罗贯中",2000,new Date(),"天下大势分久必合合久必分");
        Book book4 = new Book("14","水浒传","施耐庵",2000,new Date(),"他日若遂凌云志敢笑黄巢不丈夫");
        //转换对象为json
        ObjectMapper mapper = new ObjectMapper();
        String json1 = mapper.writeValueAsString(book1);
        String json2 = mapper.writeValueAsString(book2);
        String json3 = mapper.writeValueAsString(book3);
        String json4 = mapper.writeValueAsString(book4);
        //批量请求
        BulkRequest request = new BulkRequest(INDEX);
        //新增请求
        request.add(new IndexRequest().id(book1.getId()).source(json1,XContentType.JSON));
        request.add(new IndexRequest().id(book2.getId()).source(json2,XContentType.JSON));
        request.add(new IndexRequest().id(book3.getId()).source(json3,XContentType.JSON));
        request.add(new IndexRequest().id(book4.getId()).source(json4,XContentType.JSON));

        BulkResponse response = client.bulk(request, DEFAULT);
        System.out.println(response);
    }

    //批量更新
    void bulkDeleteBooks() throws IOException {
        BulkRequest request = new BulkRequest(INDEX);
        //DeleteRequest 批量删除数据
        request.add(new DeleteRequest().id("11"));
        request.add(new DeleteRequest().id("12"));
        request.add(new DeleteRequest().id("13"));
        request.add(new DeleteRequest().id("14"));

        BulkResponse response = client.bulk(request, DEFAULT);
        System.out.println(request);
    }
}
