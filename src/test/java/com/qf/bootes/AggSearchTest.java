package com.qf.bootes;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.GeoBoundingBoxQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.aggregations.metrics.ExtendedStats;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
public class AggSearchTest {

    private static final String INDEX="sms-logs-index";

    @Resource
    RestHighLevelClient client;

    /**
     * ## 对省份进行去重计数
     * POST sms-logs-index/_search
     * {
     * "aggs": {
     * "agg": {
     * "cardinality": {
     * "field": "province"
     * }
     * }
     * }
     * }
     */
    @Test
    void cardinality() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        builder.aggregation(AggregationBuilders.cardinality("agg").field("province"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //显示结果
        Cardinality cardinality = response.getAggregations().get("agg");
        //获取省份数量
        long value = cardinality.getValue();
        System.out.println(value);
    }

    /**
     * ## 统计fee范围
     * POST sms-logs-index/_search
     * {
     *   "aggs": {
     *     "agg": {
     *       "range": {
     *         "field": "fee",
     *         "ranges": [
     *           {
     *             "to": 5
     *           },
     *           {
     *             "from": 5,
     *             "to":10
     *           },
     *           {
     *             "from": 10
     *           }
     *         ]
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void range() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        builder.aggregation(
                AggregationBuilders.range("agg").field("fee")
                        .addUnboundedTo(5)
                        .addRange(5,10)
                        .addUnboundedFrom(10)
        );
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //显示结果
        Range range = response.getAggregations().get("agg");
        for (Range.Bucket bucket:range.getBuckets()){
            String key = bucket.getKeyAsString();
            String from= bucket.getFromAsString();
            String to=bucket.getToAsString();
            long docCount=bucket.getDocCount();
            System.out.printf("key=%s,from=%s,to=%s,count=%s\n",key,from,to,docCount);
        }
    }


    /**
     * ## 统计聚合
     * POST sms-logs-index/_search
     * {
     *   "aggs": {
     *     "agg": {
     *       "extended_stats": {
     *         "field": "fee"
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void extendedStats() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.aggregation(AggregationBuilders.extendedStats("agg").field("fee"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //显示结果
        ExtendedStats stats = response.getAggregations().get("agg");
        System.out.printf("count=%s,min=%s,max=%s",stats.getCount(),stats.getMin(),stats.getMax());
    }

    //地理位置检索
    //数据在geo text
    @Test
    void geo() throws IOException {
        SearchRequest request = new SearchRequest("map");
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        GeoBoundingBoxQueryBuilder location = QueryBuilders.geoBoundingBoxQuery("location");
        location.setCorners(34.729395,113.643117,34.722363,113.658999);
        builder.query(location);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

}
