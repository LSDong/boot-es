package com.qf.bootes;


import static org.elasticsearch.client.RequestOptions.DEFAULT;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JsonContent;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
class BootEsApplicationTests {

    public static final String BOOK = "book";
    @Resource
    RestHighLevelClient client;

    @Test
    void contextLoads() {
        System.out.println(client);
    }

    //创建索引
    @Test
    void createIndex() throws IOException {
        //1.指定settings
        Settings settings=Settings.builder()
                .put("number_of_shards",5)
                .put("number_of_replicas",1)
                .build();
        //2.指定mappings数据结构
        XContentBuilder mappings = JsonXContent.contentBuilder()
                .startObject()
                .startObject("properties")
                //开启name属性
                .startObject("name")
                .field("type","text")
                .field("index",true)
                .endObject()
                //开启author属性
                .startObject("author")
                .field("type","keyword")
                .endObject()
                //开启count属性
                .startObject("count")
                .field("type","long")
                .endObject()
                //开启on-sale属性
                .startObject("on-sale")
                .field("type","date")
                .field("format","yyyy-MM-dd HH:mm:ss||yyyy-MM-dd")
                .endObject()
                //开启descr属性
                .startObject("descr")
                .field("type","text")
                .endObject()
                .endObject()
                .endObject();

        //3.构造一个创建索引的请求
        CreateIndexRequest request = new CreateIndexRequest(BOOK).settings(settings).mapping(mappings);
        //4.发送请求
        CreateIndexResponse response = client.indices().create(request,DEFAULT);
        //5.获取响应
        System.out.println("索引创建成功:" + response.isAcknowledged());
    }

    // 查看索引是否存在
    @Test
    void existIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest(BOOK);
        boolean exists = client.indices().exists(request, DEFAULT);
        System.out.println("索引是否存在:" + exists);
    }

    //删除索引
    @Test
    void deleteIndex() throws IOException {
        AcknowledgedResponse response = client.indices().delete(new DeleteIndexRequest(BOOK),DEFAULT);
        System.out.println("删除索引成功:" + response.isAcknowledged());
    }

}
