package com.qf.bootes;

import org.assertj.core.util.Arrays;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.BoostingQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
public class OtherSearchTest {

    private static final String INDEX="sms-logs-index";

    @Resource
    RestHighLevelClient client;



    /**
     * # 模糊查询
     * ## 前缀查询 prefix
     * POST  sms-logs-index/_search
     * {
     *   "query": {
     *     "prefix": {
     *       "province": {
     *         "value": "山"
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void prefix() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.prefixQuery("province","山"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * ## fuzzy
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "fuzzy": {
     *       "corpName": {
     *         "value": "盒马鲜生",
     *         "prefix_length": 1
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void fuzzy() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.fuzzyQuery("corpName","盒马鲜生").prefixLength(1));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * ## wildcard 通配符查询
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "wildcard": {
     *       "corpName": {
     *         "value": "中国*"
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void wildcard() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.wildcardQuery("corpName","中国*"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    /**
     * #  范围查询 只针对数值类型有效
     * # fee 短信费用5-10
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "range": {
     *       "fee": {
     *         "gte": 5,
     *         "lte": 10
     *       }
     *     }
     *   }
     * }
     */
    @Test
    void range() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        builder.query(QueryBuilders.rangeQuery("fee").gte(5).lte(10));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //显示结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit:hits){
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * # regexp 正则表达式查询
     * POST /sms-logs-index/_search
     * {
     *   "query": {
     *     "regexp": {
     *       "mobile": "180[0-9]{8}"
     *     }
     *   }
     * }
     */
    @Test
    void regexp() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.regexpQuery("mobile","180[0-9]{8}"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * ## scroll 深分页查询
     * POST sms-logs-index/_search?scroll=1m
     * {
     *   "query": {
     *     "match_all": {}
     *   },
     *   "size": 2
     * }
     *
     * ## 获取下页
     * POST _search/scroll
     * {
     *   "scroll_id":"DnF1ZXJ5VGhlbkZldGNoBQAAAAAAABpnFnJjQ2phWHdqUms2TW1RdnNIOTY3SEEAAAAAAAAaaxZyY0NqYVh3alJrNk1tUXZzSDk2N0hBAAAAAAAAGmgWcmNDamFYd2pSazZNbVF2c0g5NjdIQQAAAAAAABppFnJjQ2phWHdqUms2TW1RdnNIOTY3SEEAAAAAAAAaahZyY0NqYVh3alJrNk1tUXZzSDk2N0hB",
     *   "scroll":"1m"
     * }
     *
     * ## 清空ES上下文
     * DELETE _search/scroll/scroll_id
     */
    @Test
    void scroll() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //设置ES上下文的生存时间
        request.scroll(TimeValue.timeValueMinutes(1L));
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.matchAllQuery());
        //设置每页展示三条数据
        builder.size(3);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取分页ID
        String scrollId = response.getScrollId();
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        System.out.println("====首页的内容====");
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }

        //获取下页的数据
        while (true){
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            //设置ES上下文的生存时间1分钟
            scrollRequest.scroll(TimeValue.timeValueMinutes(1L));
            //发起下一页的请求
            SearchResponse searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            //获取下页的结果
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            //判断这次查询是否有结果
            if (Arrays.isNullOrEmpty(searchHits)){
                System.out.println("====查询结果====");
                break;
            }else {
                System.out.println("====下一页====");
                for (SearchHit searchHit:searchHits){
                    System.out.println(searchHit.getSourceAsMap());
                }
            }
        }

        //清空ES上下文
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        System.out.println("清空ES上下文:" + clearScrollResponse.isSucceeded());
    }

    /**
     * ## 查询fee小于4的数据
     * POST sms-logs-index/_delete_by_query
     * {
     * "query": {
     * "range": {
     * "fee": {
     * "lt": 4
     * }
     * }
     * }
     * }
     */
    @Test
    void deleteByQuery() throws IOException {
        DeleteByQueryRequest request = new DeleteByQueryRequest(INDEX);
        request.setQuery(QueryBuilders.rangeQuery("fee").gte(5).lt(10));
        BulkByScrollResponse response = client.deleteByQuery(request, RequestOptions.DEFAULT);
        System.out.println("成功删除几条数据:" + response.getDeleted());
    }

    /**
     * # 查询省份为武汉或者北京
     * # 运营商不是联通
     * # smsContent中包含中国和平安
     * # bool查询
     *
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "bool": {
     *       "should": [
     *         {
     *           "term": {
     *             "province": {
     *               "value": "武汉"
     *             }
     *           }
     *         },
     *         {
     *           "term": {
     *             "province": {
     *               "value": "北京"
     *             }
     *           }
     *         }
     *       ],
     *       "must_not": [
     *         {
     *           "term": {
     *             "operatorId": {
     *               "value": "2"
     *             }
     *           }
     *         }
     *       ],
     *       "must": [
     *         {
     *           "match": {
     *             "smsContent": "中国"
     *           }
     *         },
     *         {
     *           "match": {
     *             "smsContent": "平安"
     *           }
     *         }
     *       ]
     *     }
     *   }
     * }
     */
    @Test
    void bool() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        //查询省份为武汉或者北京
        queryBuilder.should(QueryBuilders.termQuery("province","武汉"));
        queryBuilder.should(QueryBuilders.termQuery("province","北京"));
        //运营商是不是联通
        queryBuilder.mustNot(QueryBuilders.termQuery("operatorId",2));
        //smsContent中包含中国和平安
        queryBuilder.must(QueryBuilders.matchQuery("smsContent","中国"));
        queryBuilder.must(QueryBuilders.matchQuery("smsContent","平安"));
        //builder.query 设置查询条件的
        builder.query(queryBuilder);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * POST /sms-logs-index/_search
     * {
     *   "query": {
     *     "boosting": {
     *       "positive": {
     *         "match": {
     *           "smsContent": "收货安装"
     *         }
     *       },
     *       "negative": {
     *         "match": {
     *           "smsContent": "张三"
     *         }
     *       },
     *       "negative_boost": 0.5
     *     }
     *   }
     * }
     */
    @Test
    void boosting() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        //参数1 positive 查询
        //参数2 negative 查询
        BoostingQueryBuilder queryBuilder = new BoostingQueryBuilder(
                QueryBuilders.matchQuery("smsContent", "收获安装"),
                QueryBuilders.matchQuery("smsContent", "张三")
        ).negativeBoost(0.5f);
        //builder.query 设置查询条件的
        builder.query(queryBuilder);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * ## 查询公司名称是 盒马鲜生的
     * ## 费用小于等于5
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "bool": {
     *       "filter": [
     *         {
     *           "match": {
     *             "corpName": "盒马鲜生"
     *           }
     *         },
     *         {
     *           "range": {
     *             "fee": {
     *               "lte": 5
     *             }
     *           }
     *         }
     *       ]
     *     }
     *   }
     * }
     */
    @Test
    void filter() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //构造查询条件
        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        queryBuilder.filter(QueryBuilders.matchQuery("corpName","盒马鲜生"));
        queryBuilder.filter(QueryBuilders.rangeQuery("fee").lte(5));
        //builder.query 设置查询条件的
        builder.query(queryBuilder);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    /**
     * ## 高亮查询 短信内容中有 安装的
     * POST sms-logs-index/_search
     * {
     *   "query": {
     *     "match": {
     *       "smsContent": "安装"
     *     }
     *   },
     *   "highlight": {
     *     "fields": {
     *       "smsContent":{}
     *     },
     *     "fragment_size": 50,
     *     "pre_tags": "<font color='red' style='background-color:yellow'>",
     *     "post_tags": "</font>"
     *   }
     * }
     */
    @Test
    void highlight() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.matchQuery("smsContent","安装"));
        //query同级别设置高亮
        builder.highlighter(new HighlightBuilder().field("smsContent",10).preTags("<font color='red>").preTags("</font>"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
            HighlightField highlightField = hit.getHighlightFields().get("smsContent");
            System.out.println(highlightField);
        }
    }

}
