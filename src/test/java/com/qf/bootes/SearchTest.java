package com.qf.bootes;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
public class SearchTest {

    private static final String INDEX = "sms-logs-index";

    @Resource
    RestHighLevelClient client;

    @Test
    void term() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.termQuery("province", "北京"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }
    //terms查询
    //可以在一个属性中 查询多个值
    @Test
    void terms() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.termsQuery("province", "北京", "武汉"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }

    }
    //match_all 查询全部数据
    @Test
    void matchAll() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.matchAllQuery());
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //高阶的match查询
    //特点:可以根据要查询的属性类型 对搜索的关键字进行分词或者不分词
    @Test
    void match() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.matchQuery("smsContent","收货安装"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //布尔match查询
    @Test
    void boolMatch() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.matchQuery("smsContent","收货 安装").operator(Operator.OR));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }


    //multi_match
    //针对多个属性同时查询
    @Test
    void multiMatch() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.multiMatchQuery("北京","smsContent","province"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }

    //根据id查
    @Test
    void findById() throws IOException {
        GetRequest request = new GetRequest(INDEX, "1");
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsMap());
    }

    @Test
    void findByIds() throws IOException {
        SearchRequest request = new SearchRequest(INDEX);
        //组装各种查询提交
        SearchSourceBuilder builder = new SearchSourceBuilder();
        //builder.query 设置查询条件的
        builder.query(QueryBuilders.idsQuery().addIds("21","22","23"));
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //获取查询到的结果
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsMap());
        }
    }
}
