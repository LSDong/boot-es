package com.qf.bootes.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    private String id;

    private String name;

    private String author;

    private Integer count;

    @JsonFormat(pattern = "yyyy-MM-dd")
    // 如果实体类的属性和json对象属性名字不同 需要手动指定映射关系
    @JsonProperty("on-sale")
    private Date onSale;

    private String descr;
}
